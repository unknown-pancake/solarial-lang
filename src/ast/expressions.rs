
use super::metadata::LocationMetadata;

use crate::analysis::passes::name_resolver::ResolveMetadata;



/// AST node representing an expression.
///
#[derive(Clone, Debug)]
pub enum Expression {
  /// Variant representing an assignment expression.
  ///
  Assign {
    destination: Box<Expression>,
    operator: AssignOperator,
    value: Box<Expression>,
    location: LocationMetadata,
  },

  /// Variant representing a binary expression.
  ///
  Binary {
    left: Box<Expression>,
    operator: BinaryOperator,
    right: Box<Expression>,
    location: LocationMetadata,
  },

  /// Variant representing an unary expression.
  ///
  Unary {
    expression: Box<Expression>,
    operator: UnaryOperator,
    location: LocationMetadata,
  },

  /// Variant representing a function call.
  ///
  Call {
    function: String,
    arguments: Vec<Expression>,
    location: LocationMetadata,
    resolve_md: Option<ResolveMetadata>,
  },

  /// Variant representing a value (variable).
  ///
  Value {
    name: String,
    location: LocationMetadata,
    resolve_md: Option<ResolveMetadata>,
  },

  /// Variant representing an integer literal.
  ///
  Integer {
    value: i64,
    location: LocationMetadata,
  },

  /// Variant representing a boolean literal.
  ///
  Boolean {
    value: bool,
    location: LocationMetadata,
  },

  /// Variant representing a string literal.
  ///
  String {
    value: String,
    location: LocationMetadata,
  }
}



impl Expression {

  /// Creates a new assign expression node.
  ///
  pub fn new_assign_expression(
    destination: Expression,
    operator: AssignOperator,
    value: Expression,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Assign {
      destination: Box::new(destination),
      operator,
      value: Box::new(value),
      location: LocationMetadata::new(file, start, end),
    }
  }

  /// Creates a new binary expression node.
  ///
  pub fn new_binary_expression(
    left: Expression,
    operator: BinaryOperator,
    right: Expression,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Binary {
      left: Box::new(left),
      operator,
      right: Box::new(right),
      location: LocationMetadata::new(file, start, end),
    }
  }

  /// Creates a new unary expression node.
  ///
  pub fn new_unary_expression(
    expression: Expression,
    operator: UnaryOperator,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Unary {
      expression: Box::new(expression),
      operator,
      location: LocationMetadata::new(file, start, end)
    }
  }

  /// Creates a new call expression node.
  ///
  pub fn new_call_expression(
    function: &str,
    arguments: Vec<Expression>,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Call {
      function: function.to_owned(),
      arguments,
      location: LocationMetadata::new(file, start, end),
      resolve_md: None,
    }
  }

  /// Creates a new value literal node.
  ///
  pub fn new_value_literal(
    name: &str,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Value {
      name: name.to_owned(),
      location: LocationMetadata::new(file, start, end),
      resolve_md: None,
    }
  }

  /// Creates a new integer literal node.
  ///
  pub fn new_integer_literal(
    value: i64,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Integer {
      value,
      location: LocationMetadata::new(file, start, end)
    }
  }

  /// Creates a new boolean literal node.
  ///
  pub fn new_boolean_literal(
    value: bool,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Boolean {
      value,
      location: LocationMetadata::new(file, start, end)
    }
  }

  /// Creates a new string literal node.
  ///
  pub fn new_string_literal(
    value: String,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::String {
      value,
      location: LocationMetadata::new(file, start, end)
    }
  }



  /// Get a reference to the location metadata of the node.
  ///
  pub fn get_location(
    &self
  ) -> &LocationMetadata {
    match self {
      Self::Assign { location, .. } =>
        location,
      Self::Binary { location, .. } =>
        location,
      Self::Unary { location, .. } =>
        location,
      Self::Call { location, .. } =>
        location,
      Self::Value { location, .. } =>
        location,
      Self::Integer { location, .. } =>
        location,
      Self::Boolean { location, .. } =>
        location,
      Self::String { location, .. } =>
        location,
    }
  }

  /// Get a mutable reference to the location metadata of the node.
  ///
  pub fn get_location_mut(
    &mut self
  ) -> &mut LocationMetadata {
    match self {
      Self::Assign { location, .. } =>
        location,
      Self::Binary { location, .. } =>
        location,
      Self::Unary { location, .. } =>
        location,
      Self::Call { location, .. } =>
        location,
      Self::Value { location, .. } =>
        location,
      Self::Integer { location, .. } =>
        location,
      Self::Boolean { location, .. } =>
        location,
      Self::String { location, .. } =>
        location,
    }
  }

}



////////////////////////////////////////////////////////////////////////////////////////////////////



/// Assignment operators.
///
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum AssignOperator {
  /// `a = b`
  Set,
  /// `a += b`
  Add,
  /// `a -= b`
  Sub,
  /// `a *= b`
  Mul,
  /// `a /= b`
  Div,
  /// `a %= b`
  Mod,
  /// `a <<= b`
  BwShl,
  /// `a >>= b`
  BwShr,
  /// `a &= b`
  BwAnd,
  /// `a |= b`
  BwOr,
  /// `a ^= b`
  BwXor,
  /// `a and= b`
  And,
  /// `a or= b`
  Or
}

/// Binary operators.
///
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum BinaryOperator {
  /// `a + b`
  Add,
  /// `a - b`
  Sub,
  /// `a * b`
  Mul,
  /// `a / b`
  Div,
  /// `a % b`
  Mod,
  /// `a < b`
  Lt,
  /// `a <= b`
  Le,
  /// `a == b`
  Eq,
  /// `a != b`
  Ne,
  /// `a >= b`
  Ge,
  /// `a > b`
  Gt,
  /// `a << b`
  BwShl,
  /// `a >> b`
  BwShr,
  /// `a & b`
  BwAnd,
  /// `a | b`
  BwOr,
  /// `a ^ b`
  BwXor,
  /// `a and b`
  And,
  /// `a or b`
  Or
}

/// Unary operators.
///
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum UnaryOperator {
  /// +x
  Id,
  /// -x
  Neg,
  /// ~x
  BwNot,
  /// not x
  Not,
  /// ++x
  IncPre,
  /// x++
  IncPost,
  /// --x
  DecPre,
  /// x++
  DecPost,
  /// *x
  Deref,
  /// &x
  AddrOf
}