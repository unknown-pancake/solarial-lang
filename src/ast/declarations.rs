
use super::metadata::*;
use super::functions::FunctionSignature;
use super::blocks::Block;



/// AST node representing any top-level declaration.
///
#[derive(Clone, Debug)]
pub enum Declaration {
  /// Variant representing a function declaration.
  ///
  Function {
    signature: FunctionSignature,
    body: Block,
    location: LocationMetadata,
  }
}



impl Declaration {

  /// Creates a new function declaration node.
  ///
  pub fn new_function_declaration(
    signature: FunctionSignature,
    body: Block,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Function {
      signature, body,
      location: LocationMetadata::new(file, start, end)
    }
  }



  /// Gets a reference to the location metadata of the node.
  ///
  pub fn get_location(
    &self
  ) -> &LocationMetadata {
    match self {
      Self::Function { location, .. } =>
        location
    }
  }

  /// Gets a mutable reference to the location metadata of the node.
  ///
  pub fn get_location_mut(
    &mut self
  ) -> &mut LocationMetadata {
    match self {
      Self::Function { location, .. } =>
        location,
    }
  }

}
