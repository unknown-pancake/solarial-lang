
use super::*;


/// Trait with default implementations to walk over an AST.
///
/// AST node with walkable children are paired with at least a `enter_*` and a `exit_` method. The
/// other nodes just have a `visit_*` method.
///
pub trait Walker<E = ()> {

  /// Walk over a list of declarations.
  ///
  fn walk_declarations(
    &mut self,
    nodes: &mut Vec<Declaration>
  ) -> Result<(), E> {
    self.enter_declarations(nodes)?;

    for node in nodes.iter_mut() {
      self.walk_declaration(node)?;
    }

    self.exit_declarations(nodes)
  }

  /// Walk over a declaration.
  ///
  fn walk_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    self.enter_declaration(node)?;

    match node {
      Declaration::Function { .. } =>
        self.walk_function_declaration(node)?,
    }

    self.exit_declaration(node)
  }

  /// Walk over a function declaration
  fn walk_function_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    self.enter_function_declaration(node)?;

    if let Declaration::Function { signature,
                                   body, .. } = node {
      self.walk_function_signature(signature)?;
      self.walk_block(body)?;
    }

    self.exit_function_declaration(node)
  }

  /// Walk over a function signature.
  ///
  fn walk_function_signature(
    &mut self,
    node: &mut FunctionSignature
  ) -> Result<(), E> {
    self.enter_function_signature(node)?;

    self.walk_type(&mut node.return_type)?;

    for argument in node.arguments.iter_mut() {
      self.walk_argument_signature(argument)?;
    }

    self.exit_function_signature(node)
  }

  /// Walk over an argument signature.
  ///
  fn walk_argument_signature(
    &mut self,
    node: &mut ArgumentSignature
  ) -> Result<(), E> {
    self.enter_argument_signature(node)?;
    self.walk_type(&mut node.type_)?;
    self.exit_argument_signature(node)
  }

  /// Walk over a type.
  ///
  fn walk_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    self.enter_type(node)?;

    match node {
      Type::Value { .. } =>
        self.visit_value_type(node)?,
      Type::Pointer { .. } =>
        self.walk_pointer_type(node)?,
    }

    self.exit_type(node)
  }

  /// Walk over a pointer type.
  ///
  fn walk_pointer_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    self.enter_pointer_type(node)?;

    if let Type::Pointer { subtype, .. } = node {
      self.walk_type(subtype)?;
    }

    self.exit_pointer_type(node)
  }

  /// Walk over a block.
  ///
  fn walk_block(
    &mut self,
    node: &mut Block
  ) -> Result<(), E> {
    self.enter_block(node)?;

    for statement in node.statements.iter_mut() {
      self.walk_statement(statement)?;
    }

    self.exit_block(node)
  }

  /// Walk over a statement.
  ///
  fn walk_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    self.enter_statement(node)?;

    match node {
      Statement::Return { .. } =>
        self.walk_return_statement(node)?,
      Statement::Expression { .. } =>
        self.walk_expression_statement(node)?,
    }

    self.exit_statement(node)
  }

  /// Walk over a return statement.
  ///
  fn walk_return_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    self.enter_return_statement(node)?;

    if let Statement::Return { expression, .. } = node {
      if let Some(expression) = expression {
        self.walk_expression(expression)?;
      }
    }

    self.exit_return_statement(node)
  }

  /// Walk over an expression statement.
  ///
  fn walk_expression_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    self.enter_expression_statement(node)?;

    if let Statement::Expression { expression, .. } = node {
      self.walk_expression(expression)?;
    }

    self.exit_expression_statement(node)
  }

  /// Walk over an expression.
  ///
  fn walk_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    self.enter_expression(node)?;

    match node {
      Expression::Assign { .. } =>
        self.walk_assign_expression(node)?,
      Expression::Binary { .. } =>
        self.walk_binary_expression(node)?,
      Expression::Unary { .. } =>
        self.walk_unary_expression(node)?,
      Expression::Call { .. } =>
        self.walk_call_expression(node)?,
      Expression::Value { .. } =>
        self.visit_value_literal(node)?,
      Expression::Integer { .. } =>
        self.visit_integer_literal(node)?,
      Expression::Boolean { .. } =>
        self.visit_boolean_literal(node)?,
      Expression::String { .. } =>
        self.visit_string_literal(node)?,
    }

    self.exit_expression(node)
  }

  /// Walk over an assignment expression.
  ///
  fn walk_assign_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    self.enter_assign_expression(node)?;

    if let Expression::Assign { destination,
                                value, .. } = node {
      self.walk_expression(destination.as_mut())?;
      self.walk_expression(value.as_mut())?;
    }

    self.exit_assign_expression(node)
  }

  /// Walk over a binary expression.
  ///
  fn walk_binary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    self.enter_binary_expression(node)?;

    if let Expression::Binary { left,
                                right, .. } = node {
      self.walk_expression(left.as_mut())?;
      self.walk_expression(right.as_mut())?;
    }

    self.exit_binary_expression(node)
  }

  /// Walk over an unary expression.
  ///
  fn walk_unary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    self.enter_unary_expression(node)?;

    if let Expression::Unary { expression, .. } = node {
      self.walk_expression(expression.as_mut())?;
    }

    self.exit_unary_expression(node)
  }

  /// Walk over a call expression.
  ///
  fn walk_call_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    self.enter_call_expression(node)?;

    if let Expression::Call { arguments, .. } = node {
      for argument in arguments.iter_mut() {
        self.walk_expression(argument)?;
      }
    }

    self.exit_call_expression(node)
  }


////////////////////////////////////////////////////////////////////////////////////////////////////



  // Vec<ast::Declaration>

  /// Enter a list of declaration.
  ///
  #[allow(unused_variables)]
  fn enter_declarations(
    &mut self,
    declarations: &mut Vec<Declaration>
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a list of declaration.
  ///
  #[allow(unused_variables)]
  fn exit_declarations(
    &mut self,
    declarations: &mut Vec<Declaration>
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Declaration

  /// Enter a declaration.
  ///
  #[allow(unused_variables)]
  fn enter_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a declaration.
  ///
  #[allow(unused_variables)]
  fn exit_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Declaration::Function

  /// Enter a function declaration.
  ///
  #[allow(unused_variables)]
  fn enter_function_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a function declaration.
  #[allow(unused_variables)]
  fn exit_function_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::FunctionSignature

  /// Enter a function signature.
  ///
  #[allow(unused_variables)]
  fn enter_function_signature(
    &mut self,
    node: &mut FunctionSignature
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a function signature.
  ///
  #[allow(unused_variables)]
  fn exit_function_signature(
    &mut self,
    node: &mut FunctionSignature
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::ArgumentSignature

  /// Enter an argument signature.
  ///
  #[allow(unused_variables)]
  fn enter_argument_signature(
    &mut self,
    node: &mut ArgumentSignature
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit an argument signature.
  ///
  #[allow(unused_variables)]
  fn exit_argument_signature(
    &mut self,
    node: &mut ArgumentSignature
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Type

  /// Enter a type.
  ///
  #[allow(unused_variables)]
  fn enter_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    Ok(())
  }


  /// Exit a type.
  #[allow(unused_variables)]
  fn exit_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Type::Value

  /// Visit a value type.
  ///
  #[allow(unused_variables)]
  fn visit_value_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Type::Pointer

  /// Enter a pointer type.
  ///
  #[allow(unused_variables)]
  fn enter_pointer_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a pointer type.
  ///
  #[allow(unused_variables)]
  fn exit_pointer_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Block

  /// Enter a block.
  ///
  #[allow(unused_variables)]
  fn enter_block(
    &mut self,
    node: &mut Block
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a block.
  ///
  #[allow(unused_variables)]
  fn exit_block(
    &mut self,
    node: &mut Block
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Statement

  /// Enter a statement.
  ///
  #[allow(unused_variables)]
  fn enter_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a statement.
  ///
  #[allow(unused_variables)]
  fn exit_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Statement::Return

  /// Enter a return statement.
  ///
  #[allow(unused_variables)]
  fn enter_return_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a return statement.
  ///
  #[allow(unused_variables)]
  fn exit_return_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Statement::Expression

  /// Enter an expression statement.
  ///
  #[allow(unused_variables)]
  fn enter_expression_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit an expression statement.
  ///
  #[allow(unused_variables)]
  fn exit_expression_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression

  /// Enter an expression.
  ///
  #[allow(unused_variables)]
  fn enter_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit an expression.
  ///
  #[allow(unused_variables)]
  fn exit_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Assign

  /// Enter an assignment expression.
  ///
  #[allow(unused_variables)]
  fn enter_assign_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit an assignment expression.
  ///
  #[allow(unused_variables)]
  fn exit_assign_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Binary

  /// Enter a binary expression.
  ///
  #[allow(unused_variables)]
  fn enter_binary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a binary expression.
  #[allow(unused_variables)]
  fn exit_binary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Unary

  /// Enter an unary expression.
  ///
  #[allow(unused_variables)]
  fn enter_unary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit an unary expression.
  #[allow(unused_variables)]
  fn exit_unary_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Call

  /// Enter a call expression.
  ///
  #[allow(unused_variables)]
  fn enter_call_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  /// Exit a call expression.
  ///
  #[allow(unused_variables)]
  fn exit_call_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Value

  /// Visit a value literal.
  ///
  #[allow(unused_variables)]
  fn visit_value_literal(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Integer

  /// Visit an integer literal.
  ///
  #[allow(unused_variables)]
  fn visit_integer_literal(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::Boolean

  /// Visit a boolean literal.
  ///
  #[allow(unused_variables)]
  fn visit_boolean_literal(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }

  // ast::Expression::String

  /// Visit a string literal.
  ///
  #[allow(unused_variables)]
  fn visit_string_literal(
    &mut self,
    node: &mut Expression
  ) -> Result<(), E> {
    Ok(())
  }
}


