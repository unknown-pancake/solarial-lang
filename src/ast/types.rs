
use super::metadata::*;
use crate::analysis::passes::name_resolver::ResolveMetadata;



/// AST node representing a type.
///
#[derive(Clone, Debug)]
pub enum Type {
  /// Variant representing a value type, thus defined by a name.
  ///
  Value {
    name: String,
    location: LocationMetadata,
    resolve_md: Option<ResolveMetadata>
  },

  /// Variant representing a pointer type.
  ///
  Pointer {
    subtype: Box<Type>,
    location: LocationMetadata,
  }
}


impl Type {

  /// Creates a new value type node.
  ///
  pub fn new_value_type(
    name: &str,
    file: usize,
    start: usize,
    end: usize
  ) -> Self {
    Self::Value {
      name: name.to_owned(),
      location: LocationMetadata::new(file, start, end),
      resolve_md: None
    }
  }

  /// Creates a new pointer type node.
  ///
  pub fn new_pointer_type(
    subtype: Type,
    file: usize,
    start: usize,
    end: usize
  ) -> Self {
    Self::Pointer {
      subtype: Box::new(subtype),
      location: LocationMetadata::new(file, start, end)
    }
  }



  /// Get a reference to the location metadata of the node.
  ///
  pub fn get_location(
    &self
  ) -> &LocationMetadata {
    match self {
      Self::Value { location, .. } =>
        location,
      Self::Pointer { location, .. } =>
        location
    }
  }

  /// Get a mutable reference to the location metadata of the node.
  ///
  pub fn get_location_mut(
    &mut self
  ) -> &mut LocationMetadata {
    match self {
      Self::Value { location, .. } =>
        location,
      Self::Pointer { location, .. } =>
        location
    }
  }

}
