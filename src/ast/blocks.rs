
use super::metadata::*;
use super::statements::Statement;



/// AST node representing a block.
///
#[derive(Clone, Debug)]
pub struct Block {
  /// Statements contained within the block.
  ///
  pub statements: Vec<Statement>,
  /// Location metadata of the node.
  ///
  pub location: LocationMetadata
}



impl Block {

  /// Creates a new block node.
  ///
  pub fn new(
    statements: Vec<Statement>,
    file: usize, start: usize, end: usize,
  ) -> Self {
    Self {
      statements,
      location: LocationMetadata::new(file, start, end)
    }
  }

}