
use super::metadata::*;
use super::types::Type;

use crate::analysis::passes::name_resolver;



/// AST node representing the signature of any function.
///
#[derive(Clone, Debug)]
pub struct FunctionSignature {
  /// Return type of the function.
  ///
  pub return_type: Type,
  /// Name of the function.
  ///
  pub name: String,
  /// Arguments taken by the function.
  ///
  pub arguments: Vec<ArgumentSignature>,
  /// Location metadata of the node.
  ///
  pub location: LocationMetadata,
  /// "Defined Metadata" used by the name resolver analysis pass.
  ///
  pub defined_md: Option<name_resolver::DefinedMetadata>,
}



impl FunctionSignature {

  /// Creates a new function signature node.
  ///
  pub fn new(
    return_type: Type,
    name: &str,
    arguments: Vec<ArgumentSignature>,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self {
      return_type, arguments,
      name: name.to_owned(),
      location: LocationMetadata::new(file, start, end),
      defined_md: None,
    }
  }

}



////////////////////////////////////////////////////////////////////////////////////////////////////



/// AST node representing any argument signature within a function signature.
///
#[derive(Clone, Debug)]
pub struct ArgumentSignature {
  /// Type of the argument.
  ///
  pub type_: Type,
  /// Name of the argument.
  ///
  pub name: String,
  /// Location metadata of the node.
  ///
  pub location: LocationMetadata,
  /// "Defined Metadata" used by the name resolver analysis pass.
  ///
  pub defined_md: Option<name_resolver::DefinedMetadata>
}



impl ArgumentSignature {

  /// Creates a new argument signature node.
  ///
  pub fn new(
    type_: Type,
    name: &str,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self {
      type_,
      name: name.to_owned(),
      location: LocationMetadata::new(file, start, end),
      defined_md: None,
    }
  }

}