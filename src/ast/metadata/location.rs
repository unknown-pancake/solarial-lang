use std::ops::Range;

/// Metadata present in all AST node to locate them within a parsed file.
///
#[derive(Clone, Debug)]
pub struct LocationMetadata {
  /// File ID where the AST node is.
  ///
  pub file: usize,
  /// Index within the file (in character) the node starts.
  ///
  pub start: usize,
  /// Index within the file (in character) the node ends.
  ///
  pub end: usize,
}



impl LocationMetadata {

  /// Creates a new location metadata.
  ///
  pub fn new(
    file: usize,
    start: usize,
    end: usize
  ) -> Self {
    Self { file, start, end }
  }


  /// Gets the range of this location metadata within the file.
  ///
  pub fn range(
    &self
  ) -> Range<usize> {
    self.start .. self.end
  }

}
