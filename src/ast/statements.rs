
use super::metadata::*;
use super::expressions::Expression;


/// AST node representing a statement within a block.
///
#[derive(Clone, Debug)]
pub enum Statement {
  /// Variant representing a return statement.
  ///
  Return {
    expression: Option<Expression>,
    location: LocationMetadata,
  },

  /// Variant representing an expression.
  ///
  Expression {
    expression: Expression,
    location: LocationMetadata,
  }
}



impl Statement {

  /// Creates a new return statement node.
  ///
  pub fn new_return_statement(
    expression: Option<Expression>,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Return {
      expression,
      location: LocationMetadata::new(file, start, end)
    }
  }

  /// Creates a new expression statement node.
  ///
  pub fn new_expression_statement(
    expression: Expression,
    file: usize, start: usize, end: usize
  ) -> Self {
    Self::Expression {
      expression,
      location: LocationMetadata::new(file, start, end)
    }
  }



  /// Get a reference to the location metadata of the node.
  ///
  pub fn get_location(
    &self
  ) -> &LocationMetadata {
    match self {
      Self::Return { location, .. } =>
        location,
      Self::Expression { location, .. } =>
        location
    }
  }

  /// Get a mutable reference to the location metadata of the node.
  ///
  pub fn get_location_mut(
    &mut self
  ) -> &mut LocationMetadata {
    match self {
      Self::Return { location, .. } =>
        location,
      Self::Expression { location, .. } =>
        location
    }
  }

}
