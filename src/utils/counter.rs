

pub struct Counter {
  pub next_id: usize
}


impl Counter {

  pub fn new(
    init: usize
  ) -> Self {
    Self {
      next_id: init
    }
  }



  pub fn next(
    &mut self
  ) -> usize {
    let id = self.next_id;
    self.next_id += 1;

    id
  }

}
