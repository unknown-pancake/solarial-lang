
use std::collections::HashMap;
use std::hash::Hash;
use std::rc::Rc;



/// Represents a HashMap where each key maps to a single value, and each value maps to a single
/// key.
///
pub struct BiMap<A, B>
where
  A: Hash + PartialEq + Eq,
  B: Hash + PartialEq + Eq
{
  right: HashMap<Rc<A>, Rc<B>>,
  left: HashMap<Rc<B>, Rc<A>>,
}



impl<A, B> BiMap<A, B>
where
  A: Hash + PartialEq + Eq,
  B: Hash + PartialEq + Eq
{

  /// Creates a new bimap.
  ///
  pub fn new() -> Self {
    Self {
      right: HashMap::new(),
      left: HashMap::new(),
    }
  }



  /// Inserts a new entry in the bimap.
  ///
  pub fn insert(
    &mut self,
    left: A,
    right: B
  ) {
    let left_rc = Rc::new(left);
    let right_rc = Rc::new(right);

    self.right.insert(left_rc.clone(), right_rc.clone());
    self.left.insert(right_rc, left_rc);
  }



  /// Gets a left value given a right value.
  ///
  pub fn get_left(
    &self,
    right: &B
  ) -> Option<&Rc<A>> {
    self.left.get(right)
  }

  /// Gets a right value given a left value.
  ///
  pub fn get_right(
    &self,
    left: &A
  ) -> Option<&Rc<B>> {
    self.right.get(left)
  }

}
