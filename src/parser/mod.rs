mod grammar;
mod file_location_setter;

use peg::error::ParseError;
use peg::str::LineCol;

use grammar::solarial;
use file_location_setter::FileLocationSetter;
use crate::ast::Declaration;
use crate::ast::walker::Walker;



pub fn parse(
  file_id: usize,
  src: &str
) -> Result<Vec<Declaration>, ParseError<LineCol>> {
  let mut decls = solarial::file(src)?;
  let mut file_loc_setter = FileLocationSetter::new(file_id);

  file_loc_setter.walk_declarations(&mut decls)
    .expect("Failed to set file location!");

  Ok(decls)
}
