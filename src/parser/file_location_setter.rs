use crate::ast::*;
use crate::ast::walker::Walker;



/// Due to limitation in the tool used to parse the language, it is not possible to pass a file ID
/// at parse time. This structure implements `Walker` and walks over the AST to correct the
/// location metadata's `file` member.
///
pub struct FileLocationSetter {
  pub file: usize
}


impl FileLocationSetter {

  /// Creates a new instance.
  ///
  pub fn new(
    file: usize
  ) -> Self {
    Self {
      file
    }
  }

}



impl Walker for FileLocationSetter {

  fn enter_declaration(
    &mut self,
    node: &mut Declaration
  ) -> Result<(), ()> {
    node.get_location_mut().file = self.file;
    Ok(())
  }

  fn enter_function_signature(
    &mut self,
    node: &mut FunctionSignature
  ) -> Result<(), ()> {
    node.location.file = self.file;
    Ok(())
  }

  fn enter_argument_signature(
    &mut self,
    node: &mut ArgumentSignature
  ) -> Result<(), ()> {
    node.location.file = self.file;
    Ok(())
  }

  fn enter_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), ()> {
    node.get_location_mut().file = self.file;
    Ok(())
  }

  fn enter_block(
    &mut self,
    node: &mut Block
  ) -> Result<(), ()> {
    node.location.file = self.file;
    Ok(())
  }

  fn enter_statement(
    &mut self,
    node: &mut Statement
  ) -> Result<(), ()> {
    node.get_location_mut().file = self.file;
    Ok(())
  }

  fn enter_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), ()> {
    node.get_location_mut().file = self.file;
    Ok(())
  }

}
