
use peg;

use crate::ast;



peg::parser!{
  pub grammar solarial() for str {

    pub rule file() -> Vec<ast::Declaration>
      = _ v:declaration()*
      { v }



    rule declaration() -> ast::Declaration
      = v:function_declaration() _
      { v }
      / expected!("declaration")
    rule function_declaration() -> ast::Declaration
      = sp:position!() s:function_signature() _ b:block() ep:position!()
      { ast::Declaration::new_function_declaration(s, b, 0, sp, ep) }



    rule function_signature() -> ast::FunctionSignature
      = sp:position!() rt:type_() __ n:identifier() "(" _ args:argument_signature() ** (_ "," _) _ ")" ep:position!()
      { ast::FunctionSignature::new(rt, n, args, 0, sp, ep) }
      / expected!("function signature")
    rule argument_signature() -> ast::ArgumentSignature
      = sp:position!() ty:type_() __ n:identifier() ep:position!()
      { ast::ArgumentSignature::new(ty, n, 0, sp, ep) }
      / expected!("argument signature")



    rule type_() -> ast::Type
      = sp:position!() v:identifier() ep:position!()
      { ast::Type::new_value_type(v, 0, sp, ep) }
      / sp:position!() "*" _ t:type_() ep:position!()
      { ast::Type::new_pointer_type(t, 0, sp, ep) }
      / expected!("type")



    rule block() -> ast::Block
      = sp:position!() "{" _ s:statement()* _ "}" ep:position!()
      { ast::Block::new(s, 0, sp, ep) }



    rule statement() -> ast::Statement
      = v:return_statement() _
      { v }
      / v:expression_statement() _
      { v }
      / expected!("statement")
    rule return_statement() -> ast::Statement
      = sp:position!() "return" _ e:expression()? _ ";" ep:position!()
      { ast::Statement::new_return_statement(e, 0, sp, ep) }
    rule expression_statement() -> ast::Statement
      = sp:position!() e:expression() _ ";" ep:position!()
      { ast::Statement::new_expression_statement(e, 0, sp, ep) }



    rule expression() -> ast::Expression
      = v:assign_expression() _
      { v }
    rule assign_expression() -> ast::Expression
      = sp:position!() d:or_expression() _ op:assign_operator() _ v:assign_expression() ep:position!()
      { ast::Expression::new_assign_expression(d, op, v, 0, sp, ep) }
      / v:or_expression()
      { v }
    #[cache_left_rec]
    rule or_expression() -> ast::Expression
      = sp:position!() l:or_expression() __ "or" __ r:and_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Or, r, 0, sp, ep) }
      / v:and_expression()
      { v }
    #[cache_left_rec]
    rule and_expression() -> ast::Expression
      = sp:position!() l:and_expression() __ "and" __ r:bw_or_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::And, r, 0, sp, ep) }
      / v:bw_or_expression()
      { v }
    #[cache_left_rec]
    rule bw_or_expression() -> ast::Expression
      = sp:position!() l:bw_or_expression() _ "|" _ r:bw_xor_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::BwOr, r, 0, sp, ep) }
      / v:bw_xor_expression()
      { v }
    #[cache_left_rec]
    rule bw_xor_expression() -> ast::Expression
      = sp:position!() l:bw_xor_expression() _ "^" _ r:bw_and_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::BwXor, r, 0, sp, ep) }
      / v:bw_and_expression()
      { v }
    #[cache_left_rec]
    rule bw_and_expression() -> ast::Expression
      = sp:position!() l:bw_and_expression() _ "&" _ r:eq_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::BwAnd, r, 0, sp, ep) }
      / v:eq_expression()
      { v }
    #[cache_left_rec]
    rule eq_expression() -> ast::Expression
      = sp:position!() l:eq_expression() _ "==" _ r:cmp_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Eq, r, 0, sp, ep) }
      / sp:position!() l:eq_expression() _ "!=" _ r:cmp_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Ne, r, 0, sp, ep) }
      / v:cmp_expression()
      { v }
    #[cache_left_rec]
    rule cmp_expression() -> ast::Expression
      = sp:position!() l:cmp_expression() _ "<=" _ r:bw_shift_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Le, r, 0, sp, ep) }
      / sp:position!() l:cmp_expression() _ "<" _ r:bw_shift_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Lt, r, 0, sp, ep) }
      / sp:position!() l:cmp_expression() _ ">=" _ r:bw_shift_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Ge, r, 0, sp, ep) }
      / sp:position!() l:cmp_expression() _ ">" _ r:bw_shift_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Gt, r, 0, sp, ep) }
      / v:bw_shift_expression()
      { v }
    #[cache_left_rec]
    rule bw_shift_expression() -> ast::Expression
      = sp:position!() l:bw_shift_expression() _ "<<" _ r:term_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::BwShl, r, 0, sp, ep) }
      / sp:position!() l:bw_shift_expression() _ ">>" _ r:term_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::BwShr, r, 0, sp, ep) }
      / v:term_expression()
      { v }
    #[cache_left_rec]
    rule term_expression() -> ast::Expression
      = sp:position!() l:term_expression() _ "+" _ r:factor_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Add, r, 0, sp, ep) }
      / sp:position!() l:term_expression() _ "-" _ r:factor_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Sub, r, 0, sp, ep) }
      / v:factor_expression()
      { v }
    #[cache_left_rec]
    rule factor_expression() -> ast::Expression
      = sp:position!() l:factor_expression() _ "*" _ r:prefix_unary_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Mul, r, 0, sp, ep) }
      / sp:position!() l:factor_expression() _ "/" _ r:prefix_unary_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Div, r, 0, sp, ep) }
      / sp:position!() l:factor_expression() _ "%" _ r:prefix_unary_expression() ep:position!()
      { ast::Expression::new_binary_expression(l, ast::BinaryOperator::Mod, r, 0, sp, ep) }
      / v:prefix_unary_expression()
      { v }
    rule prefix_unary_expression() -> ast::Expression
      = sp:position!() "++" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::IncPre, 0, sp, ep) }
      / sp:position!() "+" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::Id, 0, sp, ep) }
      / sp:position!() "--" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::DecPre, 0, sp, ep) }
      / sp:position!() "-" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::Neg, 0, sp, ep) }
      / sp:position!() "not" __ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::Not, 0, sp, ep) }
      / sp:position!() "~" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::BwNot, 0, sp, ep) }
      / sp:position!() "*" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::Deref, 0, sp, ep) }
      / sp:position!() "&" _ e:suffix_unary_expression() ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::AddrOf, 0, sp, ep) }
      / v:suffix_unary_expression()
      { v }
    rule suffix_unary_expression() -> ast::Expression
      = sp:position!() e:call_expression() _ "++" ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::IncPost, 0, sp, ep) }
      / sp:position!() e:call_expression() _ "--" ep:position!()
      { ast::Expression::new_unary_expression(e, ast::UnaryOperator::DecPost, 0, sp, ep) }
      / v:call_expression()
      { v }
    #[cache]
    rule call_expression() -> ast::Expression
      = sp:position!() f:identifier() _ "(" _ a:expression() ** (_ "," _) _ ")" ep:position!()
      { ast::Expression::new_call_expression(f, a, 0, sp, ep) }
      / v:atom_expression()
      { v }
    rule atom_expression() -> ast::Expression
      = "(" _ e:expression() _ ")"
      { e }
      / sp:position!() n:identifier() ep:position!()
      { ast::Expression::new_value_literal(n, 0, sp, ep) }
      / sp:position!() v:$(digit()+) ep:position!()
      { ast::Expression::new_integer_literal(i64::from_str_radix(v, 10).unwrap(), 0, sp, ep) }
      / sp:position!() v:$("true" / "false") ep:position!()
      { ast::Expression::new_boolean_literal(v == "true", 0, sp, ep) }
      / sp:position!() "\"" v:string_char()* "\"" ep:position!()
      { ast::Expression::new_string_literal(String::from_iter(v), 0, sp, ep) }

    rule assign_operator() -> ast::AssignOperator
      = "="     { ast::AssignOperator::Set }
      / "+="    { ast::AssignOperator::Add }
      / "-="    { ast::AssignOperator::Sub }
      / "*="    { ast::AssignOperator::Mul }
      / "/="    { ast::AssignOperator::Div }
      / "%="    { ast::AssignOperator::Mod }
      / "<<="   { ast::AssignOperator::BwShl }
      / ">>="   { ast::AssignOperator::BwShr }
      / "&="    { ast::AssignOperator::BwAnd }
      / "|="    { ast::AssignOperator::BwOr }
      / "^="    { ast::AssignOperator::BwXor }
      / "and="  { ast::AssignOperator::And }
      / "or="   { ast::AssignOperator::Or }
    
    rule string_char() -> char
      = "\\n"         { '\n' }
      / "\\r"         { '\r' }
      / "\\t"         { '\t' }
      / "\\\""        { '"' }
      / "\\'"         { '\'' }
      / "\\\\"        { '\\' }
      / !"\"" v:[_]   { v }



    rule identifier() -> &'input str
      = v:$( quiet!{ (letter() / "_") (alphanum() / "_")* } )
      { v }
      / expected!("identifier")

    rule lowercase()
      = ['a' ..= 'z']
    rule uppercase()
      = ['A' ..= 'Z']
    rule letter()
      = lowercase() / uppercase()
    rule digit()
      = ['0' ..= '9']
    rule alphanum()
      = letter() / digit()



    rule whitespace()
      = quiet!{ [' ' | '\r' | '\n' | '\t']+ }
    rule comment()
      = quiet!{ "//" [^'\n']* "\n" }

    rule _
      = (whitespace() / comment())*
    rule __
      = (whitespace() / comment())+
      / expected!("whitespace")
  }
}
