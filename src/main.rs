mod analysis;
mod ast;
mod parser;
mod utils;


use codespan_reporting::diagnostic::{Diagnostic, Label};
use codespan_reporting::files::SimpleFiles;
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};

use parser::parse;



const SRC: &'static str = r#"

i4 main( i4 argc, **u1 argv ) {
  return add(1, 2);
}

i4 add( i4 x, i4 y ) {
  return x + y;
}

"#;



fn main() {
  println!("{}\n", SRC);

  let mut files = SimpleFiles::new();
  let file_id = files.add("test.sol", SRC);

  let mut diags: Vec<Diagnostic<usize>> = Vec::new();

  match parse(file_id, SRC) {
    Err(e) => {
      diags.push( Diagnostic::error()
        .with_message("Parsing error.")
        .with_labels(vec![
          Label::primary(file_id, e.location.offset..e.location.offset)
            .with_message(format!("Expected {}", e.expected))
        ])
      )
    },
    Ok(mut decls) => {
      let mut name_resolver = analysis::passes::NameResolver::new(
        &mut diags,
        true
      );

      match name_resolver.resolve(&mut decls) {
        Ok(_) => println!("{:#?}", decls),
        Err(_) => {},
      }
    }
  }

  let writer = StandardStream::stderr(ColorChoice::Always);
  let config = codespan_reporting::term::Config::default();

  for diag in &diags {
    codespan_reporting::term::emit(
      &mut writer.lock(),
      &config,
      &files,
      diag
    ).unwrap();
  }
}
