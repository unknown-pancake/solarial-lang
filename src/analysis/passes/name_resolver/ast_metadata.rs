

/// Metadata used by the name resolver analysis pass for names that has to be resolved.
///
#[derive(Clone, Debug)]
pub struct ResolveMetadata {
  pub resolved_as: Option<usize>,
}



impl ResolveMetadata {

  /// Creates a new instance.
  ///
  pub fn new() -> Self {
    Self {
      resolved_as: None,
    }
  }

}



////////////////////////////////////////////////////////////////////////////////////////////////////



/// Metadata used by the name resolver analysis pass for names that has to be defined.
///
#[derive(Clone, Debug)]
pub struct DefinedMetadata {
  pub defined_as: Option<usize>
}



impl DefinedMetadata {

  /// Creates a new instance.
  ///
  pub fn new() -> Self {
    Self {
      defined_as: None
    }
  }

}
