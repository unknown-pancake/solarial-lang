mod ast_metadata;

pub use ast_metadata::*;



use std::collections::HashMap;
use codespan_reporting::diagnostic::{Diagnostic, Label};

use crate::ast::*;
use crate::ast::metadata::LocationMetadata;
use crate::ast::walker::Walker;
use crate::utils::{BiMap, Counter};



/// Name resolver analysis pass. Resolves and defines names within the AST.
///
pub struct NameResolver<'a> {
  contexts: ContextStack,
  diagnostics: &'a mut Vec<Diagnostic<usize>>,

  unresolved: usize,
  resolved: usize,
  conflicts: usize,

  verbose: bool,
  add_undeclared_diagnostics: bool,
}



impl<'a> NameResolver<'a> {

  /// Creates a new instance.
  ///
  pub fn new(
    diagnostics: &'a mut Vec<Diagnostic<usize>>,
    verbose: bool,
  ) -> Self {
    Self {
      contexts: ContextStack::new(),
      diagnostics,
      unresolved: 0,
      resolved: 0,
      conflicts: 0,
      verbose,
      add_undeclared_diagnostics: false,
    }
  }



  /// Resolves and defines the name in the AST of the given declarations.
  ///
  /// # Note
  /// All of the declarations are considered part of the same scope.
  ///
  pub fn resolve(
    &mut self,
    declarations: &mut Vec<Declaration>
  ) -> Result<(), ()> {
    self.contexts.push_new_scope();

    self.define_builtins();

    loop {
      let last_resolved_count = self.resolved;

      self.walk_declarations(declarations)?;

      if self.unresolved == 0 || last_resolved_count == self.resolved {
        break;
      }
    }

    self.contexts.pop_scope();

    if self.unresolved > 0 || self.conflicts > 0 {
      self.add_undeclared_diagnostics = true;
      self.walk_declarations(declarations)?;

      Err(())
    } else {
      Ok(())
    }
  }



  /// Defines a name within the current scope.
  ///
  fn define_name(
    &mut self,
    name: &String,
    location: &LocationMetadata,
    defined_md: &mut Option<DefinedMetadata>
  ) {
    if let Some(_) = defined_md {
      return;
    }

    let mut md = DefinedMetadata::new();

    match self.contexts.find_name_location(name) {
      Some(loc) => {
        self.conflicts += 1;

        match loc {
          Location::Source(loc) =>
            self.diagnostics.push( Diagnostic::error()
              .with_message(format!("Name '{}' already declared.", name))
              .with_labels(vec![
                Label::primary(location.file, location.range()),
                Label::secondary(loc.file, loc.range())
                  .with_message("Previous declaration here.")
              ])
            ),
          Location::Builtin =>
            self.diagnostics.push( Diagnostic::error()
              .with_message(format!("Name '{}' already declared", name))
              .with_notes(vec![
                format!("'{}' is a builtin name.", name)
              ])
              .with_labels(vec![
                Label::primary(location.file, location.range())
              ])
            )
        }

      },
      None =>
        md.defined_as = Some(self.contexts.define_name(name, location)),
    }

    *defined_md = Some(md);
  }

  /// Resolves a name within the current scope.
  ///
  fn resolve_name(
    &mut self,
    name: &String,
    location: &LocationMetadata,
    resolve_md: &mut Option<ResolveMetadata>
  ) {
    if let Some(md) = resolve_md {
      if let Some(_) = md.resolved_as {
        return;
      }
    }

    let mut md = ResolveMetadata::new();

    match self.contexts.find_name_id(name) {
      Some(id) => {
        md.resolved_as = Some(id);
        self.resolved += 1;

        if let Some(_) = resolve_md {
          self.unresolved -= 1;
        }

        if self.verbose {
          let loc = self.contexts.find_name_id_location(id).unwrap();

          match loc {
            Location::Source(loc) =>
              self.diagnostics.push(Diagnostic::note()
                .with_message("Resolved name.")
                .with_labels(vec![
                  Label::primary(location.file, location.range())
                    .with_message("Resolved this name..."),
                  Label::secondary(loc.file, loc.range())
                    .with_message("... as this name."),
                ])
              ),
            Location::Builtin =>
              self.diagnostics.push(Diagnostic::note()
                .with_message("Resolved name.")
                .with_labels(vec![
                  Label::primary(location.file, location.range())
                    .with_message("Resolved this name as a builtin name."),
                ])
              )
          }

        }
      }
      None => if let None = resolve_md {
        self.unresolved += 1;
      }
    }

    *resolve_md = Some(md);
  }



  /// Defines a type within the current scope.
  ///
  fn define_type(
    &mut self,
    name: &String,
    location: &LocationMetadata,
    defined_md: &mut Option<DefinedMetadata>
  ) {
    if let Some(_) = defined_md {
      return;
    }

    let mut md = DefinedMetadata::new();

    match self.contexts.find_type_location(name) {
      Some(loc) => {
        self.conflicts += 1;

        match loc {
          Location::Source(loc) =>
            self.diagnostics.push( Diagnostic::error()
              .with_message(format!("Type '{}' already declared.", name))
              .with_labels(vec![
                Label::primary(location.file, location.range()),
                Label::secondary(loc.file, loc.range())
                  .with_message("Previous declaration here.")
              ])
            ),
          Location::Builtin =>
            self.diagnostics.push( Diagnostic::error()
              .with_message(format!("Type '{}' already declared", name))
              .with_notes(vec![
                format!("'{}' is a builtin type.", name)
              ])
              .with_labels(vec![
                Label::primary(location.file, location.range())
              ])
            )
        }

      },
      None =>
        md.defined_as = Some(self.contexts.define_type(name, location)),
    }

    *defined_md = Some(md);
  }

  /// Resolves a type within the current scope.
  ///
  fn resolve_type(
    &mut self,
    name: &String,
    location: &LocationMetadata,
    resolve_md: &mut Option<ResolveMetadata>
  ) {
    if let Some(md) = resolve_md {
      if let Some(_) = md.resolved_as {
        return;
      }
    }

    let mut md = ResolveMetadata::new();

    match self.contexts.find_type_id(name) {
      Some(id) => {
        md.resolved_as = Some(id);
        self.resolved += 1;

        if let Some(_) = resolve_md {
          self.unresolved -= 1;
        }

        if self.verbose {
          let loc = self.contexts.find_type_id_location(id).unwrap();

          match loc {
            Location::Source(loc) =>
              self.diagnostics.push(Diagnostic::note()
                .with_message("Resolved type.")
                .with_labels(vec![
                  Label::primary(location.file, location.range())
                    .with_message("Resolved this type..."),
                  Label::secondary(loc.file, loc.range())
                    .with_message("... as this type."),
                ])
              ),
            Location::Builtin =>
              self.diagnostics.push(Diagnostic::note()
                .with_message("Resolved type.")
                .with_labels(vec![
                  Label::primary(location.file, location.range())
                    .with_message("Resolved this type as a builtin type."),
                ])
              )
          }

        }
      }
      None => if let None = resolve_md {
        self.unresolved += 1;
      }
    }

    *resolve_md = Some(md);
  }



  /// Defines all of the builtin names/types.
  ///
  fn define_builtins(
    &mut self
  ) {
    use crate::analysis::types::BUILTIN_TYPES;

    for (id, name) in BUILTIN_TYPES.iter().enumerate() {
      self.contexts.define_builtin(name, id);
    }
  }

}



impl<'a> Walker for NameResolver<'a> {

  fn exit_function_declaration(
    &mut self,
    _: &mut Declaration
  ) -> Result<(), ()> {
    self.contexts.pop_scope();
    Ok(())
  }

  fn enter_function_signature(
    &mut self,
    node: &mut FunctionSignature,
  ) -> Result<(), ()> {
    self.define_name(&node.name, &node.location, &mut node.defined_md);

    self.contexts.push_new_scope();

    Ok(())
  }


  fn enter_argument_signature(
    &mut self,
    node: &mut ArgumentSignature
  ) -> Result<(), ()> {
    self.define_name(&node.name, &node.location, &mut node.defined_md);

    Ok(())
  }


  fn visit_value_type(
    &mut self,
    node: &mut Type
  ) -> Result<(), ()> {
    if let Type::Value { name,
                         location,
                         resolve_md, .. } = node {

      if self.add_undeclared_diagnostics {
        let md = resolve_md.as_ref().unwrap();
        if let None = md.resolved_as {
          self.diagnostics.push( Diagnostic::error()
            .with_message("Usage of an undeclared type.")
            .with_labels(vec![
              Label::primary(location.file, location.range())
            ]));
        }
      } else {
        self.resolve_type(name, location, resolve_md);
      }
    }

    Ok(())
  }

  fn enter_call_expression(
    &mut self,
    node: &mut Expression
  ) -> Result<(), ()> {
    if let Expression::Call { function,
                              location,
                              resolve_md, .. } = node {

      if self.add_undeclared_diagnostics {
        let md = resolve_md.as_ref().unwrap();
        if let None = md.resolved_as {
          self.diagnostics.push( Diagnostic::error()
            .with_message("Usage of an undeclared name.")
            .with_labels(vec![
              Label::primary(location.file, location.range())
            ]));
        }
      } else {
        self.resolve_name(function, location, resolve_md);
      }
    }


    Ok(())
  }


  fn visit_value_literal(
    &mut self,
    node: &mut Expression
  ) -> Result<(), ()> {
    if let Expression::Value { name,
                               location,
                               resolve_md, .. } = node {

      if self.add_undeclared_diagnostics {
        let md = resolve_md.as_ref().unwrap();
        if let None = md.resolved_as {
          self.diagnostics.push( Diagnostic::error()
            .with_message("Usage of an undeclared name.")
            .with_labels(vec![
              Label::primary(location.file, location.range())
            ]));
        }
      } else {
        self.resolve_name(name, location, resolve_md);
      }
    }

    Ok(())
  }

}



/// Location of a name/type.
///
enum Location {
  /// The definition of the name/type comes from a source file.
  ///
  Source(LocationMetadata),

  /// The definition is builtin.
  ///
  Builtin
}

/// Context stack, used to represent encapsulating scopes.
///
struct ContextStack {
  stack: Vec<Context>,
  name_id: Counter,
  type_id: Counter,
}

/// Represents a scope.
///
struct Context {
  name_locations: HashMap<usize, Location>,
  names: BiMap<String, usize>,

  type_locations: HashMap<usize, Location>,
  types: BiMap<String, usize>,
}


impl ContextStack {

  /// Creates a new context stack.
  ///
  fn new() -> Self {
    Self {
      stack: Vec::new(),
      name_id: Counter::new(0),
      type_id: Counter::new(0),
    }
  }



  /// Pushes a new scope to the stack.
  ///
  fn push_new_scope(&mut self) {
    self.stack.push( Context {
      name_locations: HashMap::new(),
      names: BiMap::new(),
      type_locations: HashMap::new(),
      types: BiMap::new(),
    } );
  }

  /// Pops the last scope.
  ///
  fn pop_scope(&mut self) {
    self.stack.pop();
  }



  /// Defines the given name within the current scope.
  ///
  fn define_name(
    &mut self,
    name: &String,
    location: &LocationMetadata
  ) -> usize {
    let name_id = self.name_id.next();

    let context = self.stack.last_mut().unwrap();

    context.names.insert(name.clone(), name_id);
    context.name_locations.insert(name_id, Location::Source(location.clone()));

    name_id
  }

  /// Defines the given type within the current scope.
  ///
  fn define_type(
    &mut self,
    name: &String,
    location: &LocationMetadata
  ) -> usize {
    let type_id = self.type_id.next();

    let context = self.stack.last_mut().unwrap();

    context.types.insert(name.clone(), type_id);
    context.type_locations.insert(type_id, Location::Source(location.clone()));

    type_id
  }

  /// Defines a builtin name & type. As such, both name and type shares the same ID and
  /// their location as set to [Location::Builtin].
  ///
  fn define_builtin(
    &mut self,
    name: &str,
    id: usize,
  ) {
    let context = self.stack.first_mut().unwrap();

    context.names.insert(name.to_owned(), id);
    context.name_locations.insert(id, Location::Builtin);

    context.types.insert(name.to_owned(), id);
    context.type_locations.insert(id, Location::Builtin);

    self.name_id.next_id = id + 1;
    self.type_id.next_id = id + 1;
  }



  /// Finds the ID corresponding to a name in the scope hierarchy.
  ///
  fn find_name_id(
    &self,
    name: &String
  ) -> Option<usize> {
    for context in self.stack.iter().rev() {
      if let Some(loc) = context.names.get_right(name) {
        return Some(**loc)
      }
    }

    None
  }

  /// Gets the location corresponding to a name ID in the scope hierarchy.
  ///
  fn find_name_id_location(
    &self,
    id: usize
  ) -> Option<&Location> {
    for context in self.stack.iter().rev() {
      if let Some(loc) = context.name_locations.get(&id) {
        return Some(loc)
      }
    }

    None
  }

  /// Gets the location corresponding to a name in the scope hierarchy.
  ///
  fn find_name_location(
    &self,
    name: &String
  ) -> Option<&Location> {
    for context in self.stack.iter().rev() {
      if let Some(id) = context.names.get_right(name) {
        if let Some(loc) = context.name_locations.get(id) {
          return Some(loc)
        }
      }
    }

    None
  }



  /// Finds the ID corresponding to a type in the scope hierarchy.
  ///
  fn find_type_id(
    &self,
    name: &String
  ) -> Option<usize> {
    for context in self.stack.iter().rev() {
      if let Some(loc) = context.types.get_right(name) {
        return Some(**loc)
      }
    }

    None
  }

  /// Gets the location corresponding to a type ID in the scope hierarchy.
  ///
  fn find_type_id_location(
    &self,
    id: usize
  ) -> Option<&Location> {
    for context in self.stack.iter().rev() {
      if let Some(loc) = context.type_locations.get(&id) {
        return Some(loc)
      }
    }

    None
  }

  /// Gets the location corresponding to a type in the scope hierarchy.
  ///
  fn find_type_location(
    &self,
    name: &String
  ) -> Option<&Location> {
    for context in self.stack.iter().rev() {
      if let Some(id) = context.types.get_right(name) {
        if let Some(loc) = context.type_locations.get(id) {
          return Some(loc)
        }
      }
    }

    None
  }

}


