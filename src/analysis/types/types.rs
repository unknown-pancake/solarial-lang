
use std::fmt;



/// Represents a type definition.
///
pub enum TypeDefinition {
  /// Integer type definition.
  ///
  Integer {
    /// Width of the type, in bytes.
    ///
    width: u32,
    /// True if the type is signed, false otherwise.
    ///
    signed: bool
  },

  /// Boolean type definition.
  ///
  Boolean,
}



impl TypeDefinition {

  /// Creates a new integer type definition.
  ///
  pub fn new_integer_type_definition(
    width: u32,
    signed: bool
  ) -> Self {
    Self::Integer {
      width, signed
    }
  }

  /// Creates a new boolean type definition.
  ///
  pub fn new_boolean_type_definition() -> Self {
    Self::Boolean
  }

}



impl fmt::Display for TypeDefinition {

  fn fmt(
    &self,
    f: &mut fmt::Formatter
  ) -> fmt::Result {
    match self {
      Self::Integer { width, signed } =>
        write!(f, "{}{}",
          if *signed { "i" } else { "u" },
          width
        ),
      Self::Boolean =>
        write!(f, "bool"),
    }
  }

}



////////////////////////////////////////////////////////////////////////////////////////////////////



/// Represents a type of something in the AST.
///
/// This uses a generic handle as type definitions are not owned by the AST.
///
pub enum Type<T> {
  /// Value type.
  ///
  Value(T),

  /// Pointer type.
  ///
  Pointer(Box<Type<T>>),
}



impl<T> Type<T> {

  /// Creates a new value type.
  ///
  pub fn new_value_type(
    handle: T
  ) -> Self {
    Self::Value(handle)
  }



  /// Converts the current type to a pointer type.
  ///
  pub fn pointer(
    self
  ) -> Self {
    Self::Pointer(Box::new(self))
  }

}



impl<T> Type<T>
where
  T: Eq
{

  /// Checks if both types are the same.
  ///
  /// This method assumes that if two type definition handles are equal, they are the same type.
  ///
  pub fn is_same_as(
    &self,
    other: &Type<T>
  ) -> bool {
    match self {
      Self::Value(self_handle) => match other {
        Self::Value(other_handle) =>
          self_handle.eq(other_handle),
        _ =>
          false,
      },
      Self::Pointer(self_ty) => match other {
        Self::Pointer(other_ty) =>
          self_ty.is_same_as(other_ty.as_ref()),
        _ =>
          false,
      }
    }
  }

}



impl<'a> fmt::Display for Type<&'a TypeDefinition> {

  fn fmt(
    &self,
    f: &mut fmt::Formatter
  ) -> fmt::Result {
    match self {
      Self::Value(ty) =>
        write!(f, "{}", ty),
      Self::Pointer(ty) =>
        write!(f, "*{}", ty.as_ref()),
    }
  }

}
