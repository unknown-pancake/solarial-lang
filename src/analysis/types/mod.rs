mod types;

pub use types::*;



pub const BUILTIN_TYPES: &'static [&'static str] = &[
  "u1", "u2", "u4", "u8",
  "i1", "i2", "i4", "i8",
  "usize", "isize",
  "bool",
];
